use orbrender::prelude::*;

fn main() {
    let mut window = WindowBuilder::new()
        .with_title("OrbRender - minimal example")
        .with_size(Size::new(800.0, 600.0))
        .build();

    let mut running = true;
    let mut update = true;

    loop {
        if !running {
            break;
        }

        if update {
            window.render();
            update = false;
        }

        for event in window.events() {
            match event {
                Event::System(system_event) => match system_event {
                    SystemEvent::Quit => {
                        running = false;
                    }
                    _ => {}
                },
                _ => {}
            }
        }
    }
}
