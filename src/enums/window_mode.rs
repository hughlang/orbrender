
/// Represents the mode of the window.
#[derive(Copy, Clone, PartialEq)]
pub enum WindowMode {
    /// The window occupies part of the screen, but not necessarily the entire screen.
    Normal,

    /// The window is reduced to an entry or icon on the task bar, dock, task list or desktop, depending on how the windowing system handles minimized windows.
    Minimized,

    /// The window occupies one entire screen, and the titlebar is still visible.
    Maximized,

    /// The window occupies one entire screen, is not resizable, and there is no titlebar.
    Fullscreen,

    /// The window is not visible.
    Hidden,
}