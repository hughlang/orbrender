//! This module contains the collection of important enums used in OrbRender.
pub use self::window_mode::WindowMode;

mod window_mode;