use crate::{
    backend::build_window,
    enums::WindowMode,
    structs::{Color, Icon, Size},
    traits::Window,
};

/// The `WindowBuilder` is used to build windows.
pub struct WindowBuilder {
    pub title: String,
    pub size: Size,
    pub minimum_size: Size,
    pub maximum_size: Size,
    pub mode: WindowMode,
    pub transparent: bool,
    pub always_on_top: bool,
    pub decorations: bool,
    pub icon: Option<Icon>,
    pub background: Color,
}

impl Default for WindowBuilder {
    fn default() -> Self {
        WindowBuilder {
            title: String::default(),
            size: Size::default(),
            minimum_size: Size::default(),
            maximum_size: Size::new(std::f64::MAX, std::f64::MAX),
            mode: WindowMode::default(),
            transparent: false,
            always_on_top: false,
            decorations: true,
            icon: None,
            background: Color::rgb(255, 255, 255),
        }
    }
}

impl WindowBuilder {
    /// Initializes a new `WindowBuilder` with default values.
    pub fn new() -> Self {
        WindowBuilder::default()
    }

    /// Sets the `title` of the window.
    pub fn with_title<T: Into<String>>(mut self, title: T) -> Self {
        self.title = title.into();
        self
    }

    /// Sets the intial `size` of the window.
    pub fn with_size(mut self, size: Size) -> Self {
        self.size = size;
        self
    }

    /// Sets the `minimum_size` of the window.
    pub fn with_minimum_size(mut self, minimum_size: Size) -> Self {
        self.minimum_size = minimum_size;
        self
    }

    /// Sets the `minimum_size` of the window.
    pub fn with_maximum_size(mut self, maximum_size: Size) -> Self {
        self.maximum_size = maximum_size;
        self
    }

    /// Sets the window mode to `maximized`.
    pub fn with_mode(mut self, mode: WindowMode) -> Self {
        self.mode = mode;
        self
    }

    /// Sets whether the background of the window should be transparent.
    pub fn with_transparency(mut self, transparent: bool) -> Self {
        self.transparent = transparent;
        self
    }

    /// Sets whether or not the window will always be on top of other windows.
    pub fn with_always_on_top(mut self, always_on_top: bool) -> Self {
        self.always_on_top = always_on_top;
        self
    }

    pub fn with_decorations(mut self, decorations: bool) -> Self {
        self.decorations = decorations;
        self
    }

    /// Sets the window icon.
    pub fn with_icon(mut self, icon: Icon) -> Self {
        self.icon = Some(icon);
        self
    }

    /// Sets the window `background` color.
    pub fn with_background(mut self, background: Color) -> Self {
        self.background = background;
        self
    }

    /// Build the `Window` with the given parameters.
    pub fn build(self) -> Box<Window> {
        build_window(self)
    }
}
