pub use self::runner::Runner;
pub use self::initialize::initialize;

#[cfg(target_arch = "wasm32")]
pub use self::stdweb::{build_window};

#[cfg(target_arch = "wasm32")]
mod stdweb;

#[cfg(target_arch = "wasm32")]
#[path = "initialize/stdweb.rs"]
mod initialize;

#[cfg(target_arch = "wasm32")]
#[path = "runner/stdweb.rs"]
mod runner;

#[cfg(not(target_arch = "wasm32"))]
pub use self::cairo::{build_window};

#[cfg(not(target_arch = "wasm32"))]
mod cairo;

#[cfg(not(target_arch = "wasm32"))]
#[path = "runner/default.rs"]
mod runner;

#[cfg(not(target_arch = "wasm32"))]
#[path = "initialize/default.rs"]
mod initialize;


////
//#[cfg(not(target_arch = "wasm32"))]
//pub use self::orbclient::{build_window, initialize};
//
//#[cfg(not(target_arch = "wasm32"))]
//mod orbclient;

////#[cfg(feature="webren")]
//pub use self::webrender::{build_window, initialize};
//
////#[cfg(feature="webren")]
//mod webrender;