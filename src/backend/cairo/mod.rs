use orbclient;
use orbclient::Renderer;
use rust_cairo::*;
use std::ffi::CString;

use crate::{
    events::{ElementState, Event, MouseButton, MouseEvent, SystemEvent},
    render_objects::{Image, Rectangle, RenderStorage, Text},
    structs::{Color, Point, Size},
    traits::Window,
    window::WindowBuilder,
};

unsafe fn rounded_rect(
    x: f64,
    y: f64,
    w: f64,
    h: f64,
    r: f64,
    context: *mut cairo_t,
    color: &Color,
) {
    let m_pi = 3.14159265;
    let degrees = m_pi / 180.0;

    //draw object
    cairo_new_sub_path(context);
    cairo_arc(context, x + w - r, y + r, r, -90.0 * degrees, 0.0 * degrees);
    cairo_arc(
        context,
        x + w - r,
        y + h - r,
        r,
        0.0 * degrees,
        90.0 * degrees,
    );
    cairo_arc(
        context,
        x + r,
        y + h - r,
        r,
        90.0 * degrees,
        180.0 * degrees,
    );
    cairo_arc(context, x + r, y + r, r, 180.0 * degrees, 270.0 * degrees);
    cairo_close_path(context);

    cairo_set_source_rgba(context, color.r_f(), color.g_f(), color.b_f(), color.a_f());
    cairo_fill(context);
}

pub fn build_window(window_builder: WindowBuilder) -> Box<Window> {
    Box::new(CairoWindow::from(window_builder))
}

pub fn initialize() {}

/// Backend for Cairo library.
pub struct CairoWindow {
    inner_window: orbclient::Window,
    render_storage: RenderStorage,
    background: Color,
    cairo_context: *mut _cairo,
    left_pressed: bool,
}

unsafe impl Send for CairoWindow {}

impl Window for CairoWindow {
    fn size(&self) -> Size {
        Size::new(
            self.inner_window.width() as f64,
            self.inner_window.height() as f64,
        )
    }

    fn render(&mut self) {
        // window background
        self.inner_window.set(orbclient::Color::rgba(
            self.background.r(),
            self.background.g(),
            self.background.b(),
            self.background.a(),
        ));

        for render_object in self.render_storage.into_iter() {
            unsafe {
                // Render rectangles
                if let Some(rectangle) = render_object.downcast_ref::<Rectangle>() {
                    let size = rectangle.size;
                    let position = rectangle.position;
                    let border_thickness = rectangle.border_thickness;

                    // Render border
                    if border_thickness.left > 0.0
                        || border_thickness.top > 0.0
                        || border_thickness.right > 0.0
                        || border_thickness.bottom > 0.0
                    {
                        if let Some(border_color) = rectangle.border_color {
                            if rectangle.radius > 0.0 {
                                rounded_rect(
                                    position.x,
                                    position.y,
                                    size.width,
                                    size.height,
                                    rectangle.radius,
                                    self.cairo_context,
                                    &border_color,
                                );
                            } else {
                                cairo_rectangle(
                                    self.cairo_context,
                                    position.x,
                                    position.y,
                                    size.width,
                                    size.height,
                                );

                                cairo_set_source_rgba(
                                    self.cairo_context,
                                    border_color.r_f(),
                                    border_color.g_f(),
                                    border_color.b_f(),
                                    border_color.a_f(),
                                );
                                cairo_fill(self.cairo_context);
                            }
                        }
                    }

                    if let Some(background) = rectangle.background {
                        if rectangle.radius > 0.0 {
                            rounded_rect(
                                position.x + border_thickness.left,
                                position.y + border_thickness.top,
                                size.width - border_thickness.left - border_thickness.right,
                                size.height - border_thickness.top - border_thickness.bottom,
                                rectangle.radius,
                                self.cairo_context,
                                &background,
                            );
                        } else {
                            cairo_rectangle(
                                self.cairo_context,
                                position.x + border_thickness.left,
                                position.y + border_thickness.top,
                                size.width - border_thickness.left - border_thickness.right,
                                size.height - border_thickness.top - border_thickness.bottom,
                            );

                            cairo_set_source_rgba(
                                self.cairo_context,
                                background.r_f(),
                                background.g_f(),
                                background.b_f(),
                                background.a_f(),
                            );
                            cairo_fill(self.cairo_context);
                        }
                    }
                }

                if let Some(tx) = render_object.downcast_ref::<Text>() {
                    let position = tx.position;
                    cairo_set_source_rgba(
                        self.cairo_context,
                        tx.foreground.r_f(),
                        tx.foreground.g_f(),
                        tx.foreground.b_f(),
                        tx.foreground.a_f(),
                    );

                    let font = CString::new(tx.font.family.clone()).expect("CString::new failed");
                    let text = CString::new(tx.text.clone()).expect("CString::new failed");

                    cairo_select_font_face(
                        self.cairo_context,
                        font.as_ptr(),
                        CAIRO_FONT_SLANT_NORMAL,
                        CAIRO_FONT_WEIGHT_NORMAL,
                    );
                    //                    if self.text_bold {
                    //                        cairo_select_font_face(cr, font.as_ptr(), CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
                    //                    }

                    cairo_set_font_size(self.cairo_context, tx.font.size);

                    let mut text_extens = cairo_text_extents_t {
                        x_bearing: 0.0,
                        y_bearing: 0.0,
                        width: 0.0,
                        height: 0.0,
                        x_advance: 0.0,
                        y_advance: 0.0,
                    };
                    cairo_text_extents(
                        self.cairo_context,
                        text.as_ptr(),
                        &mut text_extens as *mut cairo_text_extents_t,
                    );

                    cairo_move_to(self.cairo_context, position.x, position.y);
                    cairo_show_text(self.cairo_context, text.as_ptr());
                }

                if let Some(img) = render_object.downcast_ref::<Image>() {
                    let filename = CString::new(img.source.clone()).expect("CString::new failed");
                    let png = cairo_image_surface_create_from_png(filename.as_ptr());

                    //save cairo state
                    cairo_save(self.cairo_context);

                    //set the coordinates zero point to the desired point for the graphic
                    cairo_translate(
                        self.cairo_context,
                        img.position.x + 5.0,
                        img.position.y + 5.0,
                    );

                    //draw image on position zero for x and y
                    cairo_set_source_surface(self.cairo_context, png, 0.0, 0.0);
                    cairo_paint(self.cairo_context);

                    //restore the old cairo state
                    cairo_restore(self.cairo_context);
                }
            }
        }

        self.inner_window.sync();
    }

    fn background(&mut self, background: Color) {
        self.background = background;
    }

    fn insert_rectangle_with_z(&mut self, index: usize, rectangle: Rectangle, z: usize) -> usize {
        self.render_storage.insert_with_z(index, rectangle, z)
    }

    fn insert_text_with_z(&mut self, index: usize, text: Text, z: usize) -> usize {
        self.render_storage.insert_with_z(index, text, z)
    }

    fn insert_image_with_z(&mut self, index: usize, image: Image, z: usize) -> usize {
        self.render_storage.insert_with_z(index, image, z)
    }

    fn get_rectangle(&mut self, index: usize) -> Option<&Rectangle> {
        self.render_storage.get::<Rectangle>(index)
    }

    fn get_mut_rectangle(&mut self, index: usize) -> Option<&mut Rectangle> {
        self.render_storage.get_mut::<Rectangle>(index)
    }

    fn remove_render_object(&mut self, index: usize) {
        self.render_storage.remove(index);
    }

    fn clear(&mut self) {
        self.render_storage.clear();
    }

    fn events(&mut self) -> Vec<Event> {
        let mut events = vec![];

        for event in self.inner_window.events() {
            match event.to_option() {
                orbclient::EventOption::Quit(_) => {
                    events.push(Event::System(SystemEvent::Quit));
                }
                orbclient::EventOption::Button(button) => {
                    let state = {
                        if button.left {
                            ElementState::Pressed
                        } else {
                            ElementState::Released
                        }
                    };

                    events.push(Event::Mouse(MouseEvent::Button {
                        button: MouseButton::Left,
                        state,
                    }));
                }
                orbclient::EventOption::Mouse(mouse) => {
                    events.push(Event::Mouse(MouseEvent::Move(Point::new(
                        mouse.x as f64,
                        mouse.y as f64,
                    ))));
                }
                _ => {}
            }
        }

        events
    }
}

impl From<WindowBuilder> for CairoWindow {
    fn from(builder: WindowBuilder) -> CairoWindow {
        let mut flags = vec![];

        if builder.transparent {
            flags.push(orbclient::WindowFlag::Transparent);
        }

        if !builder.decorations {
            flags.push(orbclient::WindowFlag::Borderless);
        }

        let mut inner_window = orbclient::Window::new_flags(
            0,
            0,
            builder.size.width as u32,
            builder.size.height as u32,
            &builder.title[..],
            &flags,
        )
        .unwrap();

        let cr;
        unsafe {
            let surface = cairo_image_surface_create_for_data(
                inner_window.data_mut().as_mut_ptr() as *mut u8,
                CAIRO_FORMAT_ARGB32,
                builder.size.width as i32,
                builder.size.height as i32,
                cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, builder.size.width as i32),
            );
            cr = cairo_create(surface);

            //WHY I DO THIS?????
            //The Answer -> https://mobtowers.com/2013/04/15/html5-canvas-crisp-lines-every-time/
            //            cairo_translate(cr, 0.5, 0.5);
        }

        CairoWindow {
            inner_window,
            render_storage: RenderStorage::default(),
            background: builder.background,
            cairo_context: cr,
            left_pressed: false,
        }
    }
}
