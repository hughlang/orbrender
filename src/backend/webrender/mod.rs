use winit::WindowEvent::HiDpiFactorChanged;

use gleam::gl;
use glium::{
    glutin::{
        dpi::{LogicalPosition, LogicalSize},
        Event, WindowEvent,
    },
    SwapBuffersError,
};
use glutin::{self, GlContext};
use webrender::{self, api::*, ShaderPrecacheFlags};
use winit;

use crate::{
    enums::WindowMode,
    events::{Event as OrbEvent, SystemEvent},
    render_objects::{Rectangle, Text, RenderStorage, Image},
    structs::Color,
    traits::Window,
    window::WindowBuilder,
};

pub fn initialize() {}

pub fn build_window(window_builder: WindowBuilder) -> Box<Window> {
    Box::new(WebRenderWindow::from(window_builder))
}

fn clip_rounded(rect: LayoutRect, radius: f64) -> ComplexClipRegion {
    ComplexClipRegion::new(rect, BorderRadius::uniform(radius), ClipMode::Clip)
}

fn insert_rect(
    renderer: &mut RenderBuilder,
    rect: LayoutRect,
    color: ColorF,
    clip_rect: LayoutRect,
    radius: Option<f64>,
) {
    if let Some(radius) = radius {
        let id = renderer
            .builder
            .define_clip(rect, vec![clip_rounded(clip_rect, radius)], None);
        renderer.builder.insert_clip_id(id);
    }

    let info = PrimitiveInfo::new(rect);
    renderer.builder.insert_rect(&info, color.into());
    renderer.builder.pop_clip_id();
}

struct RenderBuilder {
    pub builder: DisplayListBuilder,
    pub resources: Vec<ResourceUpdate>,
}

struct Notifier {
    events_proxy: winit::EventsLoopProxy,
}

impl Notifier {
    fn new(events_proxy: winit::EventsLoopProxy) -> Notifier {
        Notifier { events_proxy }
    }
}

impl RenderNotifier for Notifier {
    fn clone(&self) -> Box<RenderNotifier> {
        Box::new(Notifier {
            events_proxy: self.events_proxy.clone(),
        })
    }

    fn wake_up(&self) {
        #[cfg(not(target_os = "android"))]
        let _ = self.events_proxy.wakeup();
    }

    fn new_frame_ready(
        &self,
        _: DocumentId,
        _scrolled: bool,
        _composite_needed: bool,
        _render_time: Option<u64>,
    ) {
        self.wake_up();
    }
}

pub struct WebRenderWindow {
    events_loop: winit::EventsLoop,
    inner_window: glutin::GlWindow,
    render_storage: RenderStorage,
    background: ColorF,
    render_builders: Vec<RenderBuilder>,
    renderer: webrender::Renderer,
    render_api: RenderApi,
    epoch: Epoch,
    pipeline_id: PipelineId,
    document_id: DocumentId,
    device_pixel_ratio: f64,
}

impl Window for WebRenderWindow {
    fn render(&mut self) {
        for render_object in self.render_storage.into_iter() {
            // Render rectangles
            if let Some(rectangle) = render_object.downcast_ref::<Rectangle>() {
                let bounds = euclid::TypedRect::new(
                    euclid::TypedPoint2D::new(rectangle.position.x, rectangle.position.y),
                    euclid::TypedSize2D::new(rectangle.size.width, rectangle.size.height),
                );

                let border_thickness = rectangle.border_thickness;

                if let Some(render_builder) = self.render_builders.get_mut(0) {
                    if border_thickness.left > 0.0
                        || border_thickness.top > 0.0
                        || border_thickness.right > 0.0
                        || border_thickness.bottom > 0.0
                    {
                        if rectangle.border_color.unwrap().data > 0 {
                            insert_rect(
                                render_builder,
                                bounds,
                                ColorF::new(
                                    rectangle.border_color.unwrap().r_f(),
                                    rectangle.border_color.unwrap().g_f(),
                                    rectangle.border_color.unwrap().b_f(),
                                    rectangle.border_color.unwrap().a_f(),
                                ),
                                bounds,
                                Some(rectangle.radius),
                            );
                        }

                        if rectangle.background.unwrap().data > 0 {
                            insert_rect(
                                render_builder,
                                bounds,
                                ColorF::new(
                                    rectangle.background.unwrap().r_f(),
                                    rectangle.background.unwrap().g_f(),
                                    rectangle.background.unwrap().b_f(),
                                    rectangle.background.unwrap().a_f(),
                                ),
                                euclid::TypedRect::new(
                                    euclid::TypedPoint2D::new(
                                        rectangle.position.x + rectangle.border_thickness.left,
                                        rectangle.position.y + rectangle.border_thickness.top,
                                    ),
                                    euclid::TypedSize2D::new(
                                        rectangle.size.width
                                            - border_thickness.left
                                            - border_thickness.right,
                                        rectangle.size.height
                                            - border_thickness.top
                                            - border_thickness.bottom,
                                    ),
                                ),
                                Some(rectangle.radius as f64),
                            );
                        }
                    } else {
                        if rectangle.background.unwrap().data > 0 {
                            insert_rect(
                                render_builder,
                                bounds,
                                ColorF::new(
                                    rectangle.background.unwrap().r_f(),
                                    rectangle.background.unwrap().g_f(),
                                    rectangle.background.unwrap().b_f(),
                                    rectangle.background.unwrap().a_f(),
                                ),
                                bounds,
                                Some(rectangle.radius),
                            );
                        }
                    }
                }
            }
        }

        if let Some(render_builder) = self.render_builders.pop() {
            let mut txn = Transaction::new();

            // Send webrender the size and buffer of the display

            let mut builder = render_builder.builder;
            builder.pop_stacking_context();

            let content_size = builder.content_size().clone();

            // let device_pixel_ratio = self.inner_window.get_hidpi_factor() as f64;
            let framebuffer_size = {
                let size = self
                    .inner_window
                    .get_inner_size()
                    .unwrap()
                    .to_physical(self.device_pixel_ratio as f64);
                DeviceIntSize::new(size.width as i32, size.height as i32)
            };

            // if self.blub {
            let layout_size =
                framebuffer_size.to_f64() / euclid::TypedScale::new(self.device_pixel_ratio as f64);

            let bounds = DeviceIntRect::new(DeviceIntPoint::new(0, 0), framebuffer_size);
            txn.set_window_parameters(framebuffer_size, bounds, self.device_pixel_ratio as f64);

            txn.set_display_list(
                self.epoch,
                Some(self.background),
                layout_size,
                builder.finalize(),
                true,
            );
            //     self.blub = false;
            // }

            txn.generate_frame();

            self.render_api.send_transaction(self.document_id, txn);

            self.renderer.update();
            self.renderer.render(framebuffer_size).unwrap();
            let _ = self.renderer.flush_pipeline_info();

            self.inner_window.swap_buffers().ok();

            let builder = DisplayListBuilder::new(self.pipeline_id, content_size.clone());

            let new_render_builder = RenderBuilder {
                builder: builder,
                resources: vec![],
            };

            self.render_builders.push(new_render_builder);
        }
    }

    fn background(&mut self, background: Color) {
        self.background = ColorF::new(
            background.r_f(),
            background.g_f(),
            background.b_f(),
            background.a_f(),
        );
    }

    fn insert_rectangle_with_z(&mut self, rectangle: Rectangle, z: usize) -> usize {
        self.render_storage.insert_with_z(rectangle, z)
    }

    fn insert_text_with_z(&mut self, text: Text, z: usize) -> usize {
        self.render_storage.insert_with_z(text, z)
    }

    fn insert_image_with_z(&mut self, image: Image, z: usize) -> usize {
        self.render_storage.insert_with_z(image, z)
    }

    fn remove_render_object(&mut self, index: usize) {
        self.render_storage.remove(index);
    }

    fn clear(&mut self) {
        self.render_storage.clear();
    }

    fn events(&mut self) -> Vec<OrbEvent> {
        let mut i_events = vec![];
        let mut events = vec![];

        self.events_loop.poll_events(|event| {
            i_events.push(event);
        });

        for event in &i_events {
            match event {
                winit::Event::WindowEvent { event, .. } => match event {
                    HiDpiFactorChanged(dpi) => {
                        self.device_pixel_ratio = *dpi;
                    },
                    // CloseRequested => {
                    //     events.push(OrbEvent::System(SystemEvent::Quit));
                    // },
                    _ => {}
                },
                _ => {}
            }
        };

        events
    }
}

impl From<WindowBuilder> for WebRenderWindow {
    fn from(builder: WindowBuilder) -> WebRenderWindow {
        let events_loop = winit::EventsLoop::new();
        let context_builder =
            glutin::ContextBuilder::new().with_gl(glutin::GlRequest::GlThenGles {
                opengl_version: (3, 2),
                opengles_version: (3, 0),
            });

        let miximized = builder.mode == WindowMode::Maximized;
        let _fullscreen = builder.mode == WindowMode::Fullscreen;
        let visible = builder.mode != WindowMode::Hidden;

        let window_builder = winit::WindowBuilder::new()
            .with_title(&builder.title[..])
            .with_maximized(miximized)
            .with_visibility(visible)
            .with_transparency(builder.transparent)
            .with_always_on_top(builder.always_on_top)
            .with_dimensions(winit::dpi::LogicalSize::new(
                builder.size.width as f64,
                builder.size.height as f64,
            ));

        // todo fullscreen and icon

        let inner_window =
            glutin::GlWindow::new(window_builder, context_builder, &events_loop).unwrap();

        unsafe {
            inner_window.make_current().ok();
        }

        let gl = match inner_window.get_api() {
            glutin::Api::OpenGl => unsafe {
                gl::GlFns::load_with(|symbol| inner_window.get_proc_address(symbol) as *const _)
            },
            glutin::Api::OpenGlEs => unsafe {
                gl::GlesFns::load_with(|symbol| inner_window.get_proc_address(symbol) as *const _)
            },
            glutin::Api::WebGl => unimplemented!(),
        };

        let device_pixel_ratio = inner_window.get_hidpi_factor() as f64;

        println!("OpenGL version {}", gl.get_string(gl::VERSION));
        println!("HiDPI factor {}", device_pixel_ratio);

        let options = None;

        let background = ColorF::new(
            builder.background.r_f(),
            builder.background.g_f(),
            builder.background.b_f(),
            builder.background.a_f(),
        );

        let opts = webrender::RendererOptions {
            resource_override_path: None,
            precache_flags: ShaderPrecacheFlags::EMPTY,
            device_pixel_ratio,
            clear_color: Some(background),
            //scatter_gpu_cache_updates: false,
            debug_flags: webrender::DebugFlags::ECHO_DRIVER_MESSAGES,
            ..options.unwrap_or(webrender::RendererOptions::default())
        };

        //  let frame_ready = Arc::new(AtomicBool::new(false));
        let notifier = Box::new(Notifier::new(events_loop.create_proxy()));

        let (renderer, sender) = webrender::Renderer::new(gl, notifier, opts, None).unwrap();
        // renderer.set_external_image_handler(Box::new(LimnExternalImageHandler));

        let framebuffer_size = {
            let size = inner_window
                .get_inner_size()
                .unwrap()
                .to_physical(device_pixel_ratio as f64);
            DeviceIntSize::new(size.width as i32, size.height as i32)
        };
        let api = sender.create_api();

        let document_id = api.add_document(framebuffer_size, 0);

        // renderer.set_external_image_handler(Box::new(LimnExternalImageHandler));

        let epoch = Epoch(0);

        let pipeline_id = PipelineId(0, 0);
        let mut txn = Transaction::new();
        txn.set_root_pipeline(pipeline_id);
        api.send_transaction(document_id, txn);

        let layout_size = framebuffer_size.to_f64() / euclid::TypedScale::new(device_pixel_ratio);
        let builder = DisplayListBuilder::new(pipeline_id, layout_size);

        let mut txn = Transaction::new();

        txn.set_display_list(
            epoch,
            Some(background),
            layout_size,
            builder.finalize(),
            true,
        );
        txn.set_root_pipeline(pipeline_id);
        txn.generate_frame();
        api.send_transaction(document_id, txn);

        let builder = DisplayListBuilder::new(pipeline_id, layout_size);

        WebRenderWindow {
            events_loop,
            inner_window,
            render_storage: RenderStorage::default(),
            background,
            render_builders: vec![RenderBuilder {
                builder,
                resources: vec![],
            }],
            device_pixel_ratio: device_pixel_ratio as f64,
            render_api: api,
            epoch: epoch,
            pipeline_id,
            document_id,
            renderer,
        }
    }
}
