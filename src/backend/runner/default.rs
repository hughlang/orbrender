pub struct Runner {
    run: Box<FnMut() -> bool + Send>,
}

impl Runner {
    pub fn new(run: Box<FnMut() -> bool + Send>) -> Self {
        Runner {
            run
        }
    }

    pub fn run(mut self) {
        loop {
            if !(self.run)() {
                break;
            }
        }
    }
}