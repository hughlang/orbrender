use stdweb::web::window;

pub struct Runner {
    run: Box<FnMut() -> bool + Send>,
}

impl Runner {
    pub fn new(run: Box<FnMut() -> bool + Send>) -> Self {
        Runner {
            run
        }
    }

    pub fn run(mut self) {
        (self.run)();
        window().request_animation_frame(|_| self.run());
    }
}