use std::{cell::RefCell, rc::Rc};

use stdweb::{
    _js_impl, js,
    traits::*,
    unstable::TryInto,
    web::{
        self, document, event,
        html_element::{CanvasElement, ImageElement},
        window, CanvasRenderingContext2d, FillRule,
    },
};

use crate::{
    events::{ElementState, Event, MouseButton, MouseEvent, SystemEvent},
    render_objects::{Image, Rectangle, RenderObject, RenderStorage, Text},
    structs::{Color, Point, Size},
    traits::Window,
    window::WindowBuilder,
};

fn get_mouse_button(button: web::event::MouseButton) -> MouseButton {
    match button {
        web::event::MouseButton::Left => MouseButton::Left,
        web::event::MouseButton::Wheel => MouseButton::Middle,
        web::event::MouseButton::Right => MouseButton::Right,
        _ => MouseButton::Other,
    }
}

//fn add_event_listener< T, F >(canvas: &CanvasElement, listener: F)  where T: event::ConcreteEvent, F: FnMut( T ) + 'static {
//    canvas.add_event_listener()
//}

pub fn build_window(window_builder: WindowBuilder) -> Box<Window> {
    Box::new(StdWebWindow::from(window_builder))
}

fn rounded_rect(x: f64, y: f64, w: f64, h: f64, r: f64, context: &CanvasRenderingContext2d) {
    let mut r = r;

    if w < 2.0 * r {
        r = w / 2.0;
    }
    if h < 2.0 * r {
        r = h / 2.0;
    }

    context.begin_path();
    context.move_to(x + r, y);
    context.arc_to(x + w, y, x + w, y + h, r).unwrap();
    context.arc_to(x + w, y + h, x, y + h, r).unwrap();
    context.arc_to(x, y + h, x, y, r).unwrap();
    context.arc_to(x, y, x + w, y, r).unwrap();
    context.close_path();
    context.fill(FillRule::default())
}

/// Backend for StdWeb library.
pub struct StdWebWindow {
    canvas: CanvasElement,
    context: CanvasRenderingContext2d,
    render_storage: RenderStorage,
    background: Color,
    events: Rc<RefCell<Vec<Event>>>,
}

unsafe impl Send for StdWebWindow {}

//impl StdWebWindow {
//    fn register_event_handlers(&self, canvas: CanvasElement, events: Rc<RefCell<Event>>) {
//
//    }
//}

impl Window for StdWebWindow {
    fn size(&self) -> Size {
        Size::new(self.canvas.width() as f64, self.canvas.height() as f64)
    }

    fn render(&mut self) {
        // window background
        self.context
            .set_fill_style_color(&self.background.to_string());
        self.context.fill_rect(
            0.0,
            0.0,
            self.canvas.width() as f64,
            self.canvas.height() as f64,
        );

        for render_object in self.render_storage.into_iter() {
            // Render rectangles
            if let Some(rectangle) = render_object.downcast_ref::<Rectangle>() {
                let size = rectangle.size;
                let position = rectangle.position;
                let border_thickness = rectangle.border_thickness;

                // Render border
                if border_thickness.left > 0.0
                    || border_thickness.top > 0.0
                    || border_thickness.right > 0.0
                    || border_thickness.bottom > 0.0
                {
                    if let Some(border_color) = rectangle.border_color {
                        if rectangle.radius > 0.0 {
                            self.context.set_fill_style_color(&border_color.to_string());

                            rounded_rect(
                                position.x,
                                position.y,
                                size.width,
                                size.height,
                                rectangle.radius,
                                &self.context,
                            );
                        } else {
                            self.context.set_fill_style_color(&border_color.to_string());
                            self.context
                                .fill_rect(position.x, position.y, size.width, size.height);
                        }
                    }
                }

                if let Some(background) = rectangle.background {
                    if rectangle.radius > 0.0 {
                        self.context.set_fill_style_color(&background.to_string());
                        rounded_rect(
                            position.x + border_thickness.left,
                            position.y + border_thickness.top,
                            size.width - border_thickness.left - border_thickness.right,
                            size.height - border_thickness.top - border_thickness.bottom,
                            rectangle.radius,
                            &self.context,
                        );
                    } else {
                        self.context.set_fill_style_color(&background.to_string());
                        self.context.fill_rect(
                            position.x + border_thickness.left,
                            position.y + border_thickness.top,
                            size.width - border_thickness.left - border_thickness.right,
                            size.height - border_thickness.top - border_thickness.bottom,
                        );
                    }
                }
            }

            if let Some(text) = render_object.downcast_ref::<Text>() {
                self.context
                    .set_fill_style_color(&text.foreground.to_string());
                let font_size = text.font.size as u32;
                self.context
                    .set_font(&format!("{}px {}", font_size, text.font.family));
                self.context
                    .fill_text(&text.text, text.position.x, text.position.y, None);
            }

            if let Some(image) = render_object.downcast_ref::<Image>() {
                let image_element = ImageElement::new();
                image_element.set_src(&image.source);

                js! {
                    var image = @{&image_element};
                    var context = @{&self.context};
                    image.onload = function() {
                        context.drawImage(@{&image_element}, @{&image.position.x}, @{&image.position.y});
                    };
                }
            }
        }
    }

    fn background(&mut self, background: Color) {
        self.background = background;
    }

    fn insert_rectangle_with_z(&mut self, index: usize, rectangle: Rectangle, z: usize) -> usize {
        self.render_storage.insert_with_z(index, rectangle, z)
    }

    fn insert_text_with_z(&mut self, index: usize, text: Text, z: usize) -> usize {
        self.render_storage.insert_with_z(index, text, z)
    }

    fn insert_image_with_z(&mut self, index: usize, image: Image, z: usize) -> usize {
        self.render_storage.insert_with_z(index, image, z)
    }

    fn get_rectangle(&mut self, index: usize) -> Option<&Rectangle> {
        self.render_storage.get::<Rectangle>(index)
    }

    fn get_mut_rectangle(&mut self, index: usize) -> Option<&mut Rectangle> {
        self.render_storage.get_mut::<Rectangle>(index)
    }

    fn remove_render_object(&mut self, index: usize) {
        self.render_storage.remove(index);
    }

    fn clear(&mut self) {
        self.render_storage.clear();
    }

    fn events(&mut self) -> Vec<Event> {
        let mut events = vec![];
        events.append(&mut *self.events.borrow_mut());
        events
    }
}

impl From<WindowBuilder> for StdWebWindow {
    fn from(builder: WindowBuilder) -> StdWebWindow {
        let canvas: CanvasElement = document()
            .create_element("canvas")
            .unwrap()
            .try_into()
            .unwrap();

        canvas.set_width(builder.size.width as u32);
        canvas.set_height(builder.size.height as u32);

        js! {
            document.body.style.padding = 0;
            document.body.style.margin = 0;
            @{&canvas}.style.display = "block";
            @{&canvas}.style.margin = "0";
        }

        document().body().unwrap().append_child(&canvas);

        let events = Rc::new(RefCell::new(vec![]));

        document().set_title(&builder.title[..]);
        let context: CanvasRenderingContext2d = canvas.get_context().unwrap();

        let devicePixelRatio = window().device_pixel_ratio();

        let backingStoreRatio = js! {
            var context = @{&context};
             return context.webkitBackingStorePixelRatio ||
                 context.mozBackingStorePixelRatio ||
                 context.msBackingStorePixelRatio ||
                 context.oBackingStorePixelRatio ||
                 context.backingStorePixelRatio || 1;
        };

        let ratio: f64 = js! {
            return @{&devicePixelRatio} / @{&backingStoreRatio};
        }
        .try_into()
        .unwrap();

        if devicePixelRatio != backingStoreRatio {
            let old_width = canvas.width();
            let old_height = canvas.height();
            canvas.set_width((old_width as f64 * ratio) as u32);
            canvas.set_height((old_height as f64 * ratio) as u32);

            js! {
                @{&canvas}.style.width = @{&old_width} + "px";
                @{&canvas}.style.height = @{&old_height} + "px";
            }

            context.scale(ratio, ratio);
        }

        let events_c = events.clone();
        window().add_event_listener(move |e: event::MouseDownEvent| {
            events_c.borrow_mut().push(Event::Mouse(MouseEvent::Button {
                button: get_mouse_button(e.button()),
                state: ElementState::Pressed,
            }));
        });

        let events_c = events.clone();
        window().add_event_listener(move |e: event::MouseUpEvent| {
            events_c.borrow_mut().push(Event::Mouse(MouseEvent::Button {
                button: get_mouse_button(e.button()),
                state: ElementState::Released,
            }));
        });

        let events_c = events.clone();
        canvas.add_event_listener(move |e: event::MouseMoveEvent| {
            events_c
                .borrow_mut()
                .push(Event::Mouse(MouseEvent::Move(Point::new(
                    e.client_x() as f64,
                    e.client_y() as f64,
                ))));
        });

        stdweb::event_loop();

        StdWebWindow {
            canvas,
            context,
            render_storage: RenderStorage::default(),
            background: builder.background,
            events,
        }
    }
}
