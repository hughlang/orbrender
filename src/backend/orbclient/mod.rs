use orbclient;
use orbclient::Renderer;

use crate::{
    events::{Event, SystemEvent},
    render_objects::{Rectangle, Text, RenderStorage, Image},
    structs::Color,
    traits::Window,
    window::WindowBuilder,
};

pub fn build_window(window_builder: WindowBuilder) -> Box<Window> {
    Box::new(OrbClientWindow::from(window_builder))
}

pub fn initialize() {
}

/// Backend for OrbClient library.
pub struct OrbClientWindow {
    inner_window: orbclient::Window,
    render_storage: RenderStorage,
    background: Color,
}

impl Window for OrbClientWindow {
    fn render(&mut self) {
        // window background
        self.inner_window.set(orbclient::Color::rgba(
            self.background.r(),
            self.background.g(),
            self.background.b(),
            self.background.a(),
        ));

        for render_object in self.render_storage.into_iter() {
            // Render rectangles
            if let Some(rectangle) = render_object.downcast_ref::<Rectangle>() {
                let size = rectangle.size;
                let position = rectangle.position;
                let border_thickness = rectangle.border_thickness;

                // Render border
                if border_thickness.left > 0.0
                    || border_thickness.top > 0.0
                    || border_thickness.right > 0.0
                    || border_thickness.bottom > 0.0
                {
                    if let Some(border_color) = rectangle.border_color {
                        if rectangle.radius > 0.0 {
                            self.inner_window.rounded_rect(
                                position.x as i32,
                                position.y as i32,
                                size.width as u32,
                                size.height as u32,
                                rectangle.radius as u32,
                                true,
                                orbclient::Color::rgba(
                                    border_color.r(),
                                    border_color.g(),
                                    border_color.b(),
                                    border_color.a(),
                                ),
                            )
                        } else {
                            self.inner_window.rect(
                                position.x as i32,
                                position.y as i32,
                                size.width as u32,
                                size.height as u32,
                                orbclient::Color::rgba(
                                    border_color.r(),
                                    border_color.g(),
                                    border_color.b(),
                                    border_color.a(),
                                ),
                            )
                        }
                    }
                }

                if let Some(background) = rectangle.background {
                    if rectangle.radius > 0.0 {
                        self.inner_window.rounded_rect(
                            (position.x + border_thickness.left) as i32,
                            (position.y + border_thickness.top) as i32,
                            (size.width - border_thickness.left - border_thickness.right) as u32,
                            (size.height - border_thickness.top - border_thickness.bottom) as u32,
                            rectangle.radius as u32,
                            true,
                            orbclient::Color::rgba(
                                background.r(),
                                background.g(),
                                background.b(),
                                background.a(),
                            ),
                        )
                    } else {
                        self.inner_window.rect(
                            (position.x + border_thickness.left) as i32,
                            (position.y + border_thickness.top) as i32,
                            (size.width - border_thickness.left - border_thickness.right) as u32,
                            (size.height - border_thickness.top - border_thickness.bottom) as u32,
                            orbclient::Color::rgba(
                                background.r(),
                                background.g(),
                                background.b(),
                                background.a(),
                            ),
                        )
                    }
                }
            }
        }

        self.inner_window.sync();
    }

    fn background(&mut self, background: Color) {
        self.background = background;
    }

    fn insert_rectangle_with_z(&mut self, rectangle: Rectangle, z: usize) -> usize {
        self.render_storage.insert_with_z(rectangle, z)
    }

    fn insert_text_with_z(&mut self, text: Text, z: usize) -> usize {
        self.render_storage.insert_with_z(text, z)
    }

    fn insert_image_with_z(&mut self, image: Image, z: usize) -> usize {
        self.render_storage.insert_with_z(image, z)
    }

    fn remove_render_object(&mut self, index: usize) {
        self.render_storage.remove(index);
    }

    fn clear(&mut self) {
        self.render_storage.clear();
    }

    fn events(&mut self) -> Vec<Event> {
        let mut events = vec![];

        for event in self.inner_window.events() {
            match event.to_option() {
                orbclient::EventOption::Quit(_) => {
                    events.push(Event::System(SystemEvent::Quit));
                }
                _ => {}
            }
        }

        events
    }
}

impl From<WindowBuilder> for OrbClientWindow {
    fn from(builder: WindowBuilder) -> OrbClientWindow {
        let mut flags = vec![];

        if builder.transparent {
            flags.push(orbclient::WindowFlag::Transparent);
        }

        if !builder.decorations {
            flags.push(orbclient::WindowFlag::Borderless);
        }

        let inner_window = orbclient::Window::new_flags(
            0,
            0,
            builder.size.width as u32,
            builder.size.height as u32,
            &builder.title[..],
            &flags,
        )
        .unwrap();

        OrbClientWindow {
            inner_window,
            render_storage: RenderStorage::default(),
            background: builder.background,
        }
    }
}
