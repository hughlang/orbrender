use crate::structs::Point;
use super::ElementState;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum MouseButton {
    Left,
    Right,
    Middle,
    Other,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum MouseEvent {
    Button { button: MouseButton, state: ElementState },
    Move(Point)
}

