pub use self::common::*;
pub use self::mouse::{MouseEvent, MouseButton};
pub use self::system::SystemEvent;

mod common;
mod mouse;
mod system;

pub enum Event {
    Mouse(MouseEvent),
    System(SystemEvent),
}

