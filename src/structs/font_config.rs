/// `FontConfig` describes the font configuration of a text.
#[derive(Default)]
pub struct FontConfig {
    pub family: String,
    pub size: f64,
    pub weight: String,
    pub style: String,
}

impl FontConfig {
    /// Sets the `family` color of the font config.
    pub fn with_family<F: Into<String>>(mut self, family: F) -> Self {
        self.family = family.into();
        self
    }

    /// Sets the `size` color of the font config.
    pub fn with_size(mut self, size: f64) -> Self {
        self.size = size;
        self
    }

    /// Sets the `weight` color of the font config.
    pub fn with_weight(mut self, weight: String) -> Self {
        self.weight = weight;
        self
    }

    /// Sets the `style` color of the font config.
    pub fn with_style(mut self, style: String) -> Self {
        self.style = style;
        self
    }
}
