/// `Thickness` describes a frame.
#[derive(Default, Copy, Clone, PartialEq)]
pub struct Thickness {
    pub left: f64,
    pub top: f64,
    pub right: f64,
    pub bottom: f64,
}

impl Thickness {
    /// Sets `left`.
    pub fn with_left(mut self, left: f64) -> Self {
        self.left = left;
        self
    }

    /// Sets `top`.
    pub fn with_top(mut self, top: f64) -> Self {
        self.top = top;
        self
    }

    /// Sets `right`.
    pub fn with_right(mut self, right: f64) -> Self {
        self.right = right;
        self
    }

    /// Sets `bottom`.
    pub fn with_bottom(mut self, bottom: f64) -> Self {
        self.bottom = bottom;
        self
    }

    /// Sets `top` and `bottom` to the same value.
    pub fn with_top_bottom(mut self, value: f64) -> Self {
        self.top = value;
        self.bottom = value;
        self
    }

    /// Sets `left` and `right` to the same value.
    pub fn with_left_right(mut self, value: f64) -> Self {
        self.left = value;
        self.right = value;
        self
    }

    // Sets `left`, `right`, `top` and `bottom` to the same value.
    pub fn with_all(mut self, value: f64) -> Self {
        self.left = value;
        self.top = value;
        self.right = value;
        self.bottom = value;
        self
    }
}
