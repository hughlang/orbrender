use crate::enums::WindowMode;

/// An icon used for the window titlebar, taskbar, etc.
#[derive(Default)]
pub struct Icon {}

impl Default for WindowMode {
    fn default() -> Self {
        WindowMode::Normal
    }
}