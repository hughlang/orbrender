/// Represents the size of a visual object.
#[derive(Default, Copy, Clone, PartialEq)]
pub struct Size {
    pub width: f64,
    pub height: f64,
}

impl Size {
    /// Initializes a new `Size` with `width` and `height`.
    pub fn new(width: f64, height: f64) -> Self {
        Size { width, height }
    }
}