//! This module contains non visual structures like size and icon.
pub use self::color::Color;
pub use self::font_config::FontConfig;
pub use self::icon::Icon;
pub use self::point::Point;
pub use self::size::Size;
pub use self::thickness::Thickness;

mod color;
mod font_config;
mod icon;
mod point;
mod size;
mod thickness;