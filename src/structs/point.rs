/// Represents the point with `x` and `y`.
#[derive(Default, Debug, PartialEq, Copy, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Point {
    /// Initializes a new `Point` with `x` and `y`.
    pub fn new(x: f64, y: f64) -> Self {
        Point { x, y }
    }
}