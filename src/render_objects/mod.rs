use std::any::{Any, TypeId};
use std::collections::{btree_map::Iter, btree_set, BTreeMap, BTreeSet};

pub use self::image::Image;
pub use self::rectangle::Rectangle;
pub use self::text::Text;

mod image;
mod rectangle;
mod text;

/// This trait is used to internal handle all render object.
pub trait RenderObject: Any {}

/// Used to store render objects.
#[derive(Default)]
pub struct RenderStorage {
    render_objects: BTreeMap<usize, Box<Any>>,
    z_map: BTreeMap<usize, BTreeSet<usize>>,
}

impl RenderStorage {
    /// Appends an render object with z index to the storage. Returns the id of the render object.
    pub fn insert_with_z<R: RenderObject>(&mut self, index: usize, render_object: R, z: usize) -> usize {
        self.render_objects
            .insert(index, Box::new(render_object));

        if !self.z_map.contains_key(&z) {
            self.z_map.insert(z, BTreeSet::new());
        }

        // Unwrap because if the key does not exists it will be set one step before
        self.z_map
            .get_mut(&z)
            .unwrap()
            .insert(index);

        index
    }

    /// Returns a reference to a render object depending on the type of index.
    pub fn get<R: RenderObject>(&mut self, index: usize) -> Option<&R>  {
        if let Some(render_object) = self.render_objects.get_mut(&index) {
            if let Some(render_object) = render_object.downcast_ref::<R>() {
                return Some(render_object);
            }
        }

        None
    }

    /// Returns a mutable reference to a render object depending on the type of index.
    pub fn get_mut<R: RenderObject>(&mut self, index: usize) -> Option<&mut R>  {
        if let Some(render_object) = self.render_objects.get_mut(&index) {
            if let Some(render_object) = render_object.downcast_mut::<R>() {
                return Some(render_object);
            }
        }

        None
    }

    /// Removes the render object with the given `index` from the storage.
    pub fn remove(&mut self, index: usize) {
        self.render_objects.remove(&index);
        self.z_map
            .iter_mut()
            .filter(|(_, m)| m.contains(&index))
            .for_each(|(_, m)| {
                m.remove(&index);
            });
    }

    /// Removes all render objects from the storage.
    pub fn clear(&mut self) {
        self.render_objects.clear();
        self.z_map.clear();
    }
}

impl<'a> IntoIterator for &'a RenderStorage {
    type Item = &'a Box<Any>;
    type IntoIter = RenderStorageIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RenderStorageIterator {
            storage: self,
            z_map_iter: None,
            render_objects_iter: None,
        }
    }
}

/// Used to create an iterator for the render storage.
pub struct RenderStorageIterator<'a> {
    storage: &'a RenderStorage,
    z_map_iter: Option<Iter<'a, usize, BTreeSet<usize>>>,
    render_objects_iter: Option<btree_set::Iter<'a, usize>>,
}

impl<'a> Iterator for RenderStorageIterator<'a> {
    type Item = &'a Box<Any>;

    fn next(&mut self) -> Option<&'a Box<Any>> {
        if self.z_map_iter.is_none() {
            self.z_map_iter = Some(self.storage.z_map.iter());
        }

        loop {
            if let Some(z_map_iter) = &mut self.z_map_iter {
                if self.render_objects_iter.is_none() {
                    if let Some((_, map)) = &z_map_iter.next() {
                        self.render_objects_iter = Some(map.iter());
                    } else {
                        return None;
                    }
                }
            }

            if let Some(render_objects_iter) = &mut self.render_objects_iter {
                if let Some(i) = render_objects_iter.next() {
                    if let Some(render_object) = &self.storage.render_objects.get(i) {
                        return Some(render_object);
                    }
                } else {
                    self.render_objects_iter = None;
                }
            }
        }
    }
}
