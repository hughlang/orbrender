use crate::{
    render_objects::RenderObject,
    structs::{Color, FontConfig, Point},
};

/// The `Text` is used to draw a text on the screen.
#[derive(Default)]
pub struct Text {
    pub foreground: Color,
    pub font: FontConfig,
    pub text: String,
    pub position: Point,
}

impl Into<Box<RenderObject>> for Text {
    fn into(self) -> Box<RenderObject> {
        Box::new(self)
    }
}

impl Text {
    /// Sets the `foreground` color of the text.
    pub fn with_foreground(mut self, foreground: Color) -> Self {
        self.foreground = foreground;
        self
    }

    /// Sets the `font` of the text.
    pub fn with_font(mut self, font: FontConfig) -> Self {
        self.font = font;
        self
    }

    /// Sets the `text` of the text.
    pub fn with_text<T: Into<String>>(mut self, text: T) -> Self {
        self.text = text.into();
        self
    }

     /// Sets the `position` of the text.
    pub fn with_position(mut self, position: Point) -> Self {
        self.position = position;
        self
    }
}

impl RenderObject for Text {}
