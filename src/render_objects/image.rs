use crate::{
    render_objects::RenderObject,
    structs::{Point, Size},
};

/// The `Image` is used to draw a image on the screen.
#[derive(Default)]
pub struct Image {
    pub source: String,
    pub size: Size,
    pub position: Point,
    pub index: usize,
}

impl Into<Box<RenderObject>> for Image {
    fn into(self) -> Box<RenderObject> {
        Box::new(self)
    }
}

impl Image {
     /// Sets the `size` of the image.
    pub fn with_source<S: Into<String>>(mut self, source: S) -> Self {
        self.source = source.into();
        self
    }

    /// Sets the `size` of the image.
    pub fn with_size(mut self, size: Size) -> Self {
        self.size = size;
        self
    }

    /// Sets the `position` of the image.
    pub fn with_position(mut self, position: Point) -> Self {
        self.position = position;
        self
    }
}

impl RenderObject for Image {}
