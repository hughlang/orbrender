use crate::{
    render_objects::RenderObject,
    structs::{Color, Point, Size, Thickness},
};

/// The `Rectangle` is used to draw a rectangle on the screen.
#[derive(Default)]
pub struct Rectangle {
    pub background: Option<Color>,
    pub border_color: Option<Color>,
    pub border_thickness: Thickness,
    pub radius: f64,
    pub size: Size,
    pub position: Point,
}

impl Into<Box<RenderObject>> for Rectangle {
    fn into(self) -> Box<RenderObject> {
        Box::new(self)
    }
}

impl Rectangle {
    /// Sets the `background` color of the rectangle.
    pub fn with_background(mut self, background: Color) -> Self {
        self.background = Some(background);
        self
    }

    /// Sets the `border_color` of the rectangle.
    pub fn with_border_color(mut self, border_color: Color) -> Self {
        self.border_color = Some(border_color);
        self
    }

    /// Sets the `border_thickness` of the rectangle.
    pub fn with_border_thickness(mut self, border_thickness: Thickness) -> Self {
        self.border_thickness = border_thickness;
        self
    }

    /// Sets the `radius` of the rectangle.
    pub fn with_radius(mut self, radius: f64) -> Self {
        self.radius = radius;
        self
    }

    /// Sets the `size` of the rectangle.
    pub fn with_size(mut self, size: Size) -> Self {
        self.size = size;
        self
    }

    /// Sets the `position` of the rectangle.
    pub fn with_position(mut self, position: Point) -> Self {
        self.position = position;
        self
    }
}

impl RenderObject for Rectangle {}
