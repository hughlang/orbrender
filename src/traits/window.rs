use crate::{
    events::Event,
    render_objects::{Rectangle, RenderObject, Text, Image},
    structs::{Color, Size},
};

pub trait Window : Send {
    /// Returns the size of the window.
    fn size(&self) -> Size;

    /// Renders all render objects of the window.
    fn render(&mut self);

    /// Sets the `background` color of the window.
    fn background(&mut self, color: Color);

    /// Inserts a rectangle render object with z index into the render map. Returns the index of the rectangle.
    fn insert_rectangle_with_z(&mut self, index: usize, rectangle: Rectangle, z: usize) -> usize;

    /// Inserts a rectangle render object into the render map. Returns the index of the rectangle.
    fn insert_rectangle(&mut self, index: usize, rectangle: Rectangle) -> usize {
        self.insert_rectangle_with_z(index, rectangle, 0)
    } 

    /// Inserts a text render object with z index into the render map. Returns the index of the text.
    fn insert_text_with_z(&mut self, index: usize, text: Text, z: usize) -> usize;

    /// Inserts a text render object into the render map. Returns the index of the text.
    fn insert_text(&mut self, index: usize, text: Text) -> usize {
        self.insert_text_with_z(index, text, 0)
    } 

    /// Inserts a image render object with z index into the render map. Returns the index of the image.
    fn insert_image_with_z(&mut self, index: usize, image: Image, z: usize) -> usize;

    /// Inserts a image render object into the render map. Returns the index of the image.
    fn insert_image(&mut self, index: usize, image: Image) -> usize {
        self.insert_image_with_z(index, image, 0)
    }

    /// Returns a reference to a rectangle depending on the type of index.
    fn get_rectangle(&mut self, index: usize) -> Option<&Rectangle>;

    /// Returns a mutable reference to a rectangle depending on the type of index.
    fn get_mut_rectangle(&mut self, index: usize) -> Option<&mut Rectangle>;

    /// Remove a render object (rectangle, image, text) by the given index.
    fn remove_render_object(&mut self, index: usize);

    /// Clears all render objects.
    fn clear(&mut self);

    /// Request events.
    fn events(&mut self) -> Vec<Event>;
}
