# OrbRender

[![Build status](https://gitlab.redox-os.org/FloVanGH/orbrender/badges/master/build.svg)](https://gitlab.redox-os.org/FloVanGH/orbrender/pipelines)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)

The aim of OrbRender is to provide an uniform API for window creation, window drawing and request of window events. OrbRender could work with different client
libraries like [OrbClient](https://gitlab.redox-os.org/redox-os/orbclient), [winit](https://github.com/tomaka/winit), [WebRender](https://github.com/servo/webrender), 
[rust-cairo](https://gitlab.redox-os.org/redox-os/rust-cairo) and [stdweb](https://github.com/koute/stdweb).

> 3D rendering features will follow

## Primary target platforms

* Redox OS
* Linux
* Windows
* macOS
* Web

## Planned target platforms

* Android
* iOS

## Usage

To include OrbRender in your project, just add the dependency
line to your `Cargo.toml` file:

```text
orbrender = { git = https://gitlab.redox-os.org/FloVanGH/orbrender.git }
```

## Web setup

To run the examples on a browser you have to install 

```text
cargo install -f cargo-web
```

## Examples

You find the examples in the `examples/` directory.

You can start the drawing example by executing the following command:

```text
cargo run --example drawing --release
```

* Compile to [WebAssembly](https://en.wikipedia.org/wiki/WebAssembly) using Rust's native WebAssembly backend:

```text
cargo web start --target=wasm32-unknown-unknown --auto-reload --example drawing
```

* Compile to [asm.js](https://en.wikipedia.org/wiki/Asm.js) using Emscripten:

```text
$ cargo web start --target=asmjs-unknown-emscripten --auto-reload --example drawing
```

* Compile to WebAssembly using Emscripten:

```text
$ cargo web start --target=wasm32-unknown-emscripten --auto-reload --example drawing
```

## Build and run documenation

You can build and run the latest documentation y executing the following command:

```text
cargo doc --no-deps --open
```